module gitlab.com/richard985/loggar

go 1.21

require github.com/go-kit/log v0.2.1

require github.com/go-logfmt/logfmt v0.5.1 // indirect
