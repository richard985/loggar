package backend_logger

import (
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"os"
)

var Logger log.Logger

func init() {
	Logger = log.NewLogfmtLogger(os.Stderr)
	Logger = log.With(Logger, "ts", log.DefaultTimestampUTC)
}

func Log(id string, service, message, fnc string) {
	// Registrar el mensaje con el formato personalizado
	level.Info(Logger).Log("[srv]", service, "[id]", id, "[fnc]", fnc, "[msg]", message)
}

func LogError(id string, service, message, fnc string, err error) {
	// Registrar el mensaje de error con el formato personalizado
	level.Error(Logger).Log("[srv]", service, "[id]", id, "[fnc]", fnc, "[msg]", message, "[err]", err)
}

func LogWarn(id string, service, message, fnc string) {
	// Registrar el mensaje de warn con el formato personalizado
	level.Warn(Logger).Log("[srv]", service, "[id]", id, "[fnc]", fnc, "[msg]", message)
}

func LogDebug(id string, service, message, fnc string) {
	// Registrar el mensaje de debug con el formato personalizado
	level.Debug(Logger).Log("[srv]", service, "[id]", id, "[fnc]", fnc, "[msg]", message)
}
